
# Load libraries ------------------
require(tidyverse)
require(ncdf4)
require(lubridate)
require(furrr)
require(pracma)
require(geosphere)
# Custom functions ----------

grav <- function(lat){
  # This expression is taken from Wang et al. 2016.
  # It references Mahoney (2001). It seems the equation has a typo
  #9.780325*sqrt( (1+0.00193185*(sin(lat*acos(-1)/180))^2)/(1-0.00669435*(sin(lat*acos(-1)/180))^2))
  9.780325*( (1+0.00193185*(sin(lat*acos(-1)/180))^2)/sqrt(1-0.00669435*(sin(lat*acos(-1)/180))^2))
  # in m s^{-2}

  # Citation:
  # Wang, X., K. Zhang, S. Wu, S. Fan, and
  # Y. Cheng (2016), Water vapor-weighted
  # mean temperature and its impact on
  # the determination of precipitable water
  # vapor and its linear trend, J. Geophys. Res.
  # Atmos., 121, 833–852, doi:10.1002/2015JD024181.
  #
  # Mahoney (2001)
  # https://wahiduddin.net/calc/refs/measures_of_altitude_mahoney.html
}
radius <- function(lat){
  6378.137/(1.006803-0.006706*(sin(lat*acos(-1)/180))^2)
}
gph_from_hgeoid <- function(hg, lat){
  grav45 = 9.80665 #m s^{-2}
  gph= (grav(lat)/grav45)*(radius(lat)*hg/(radius(lat)+hg))
}

hgeoid_from_gp <- function(gp, lat){
  # From https://unidata.github.io/MetPy/latest/api/generated/metpy.calc.geopotential_to_height.html
  h_geoid=gp*radius(lat)/(grav(lat)*radius(lat)-gp)
}
reduce_pressure <- function(pc,tc,gamma,H, Hc,lat){
  #Version adapted from https://agupubs.onlinelibrary.wiley.com/doi/full/10.1002/2015JD024181
  # pc, tc, Hc are pressure, temperature and altitude from reanalysis
  # H is the altitude of GNSS station
  #Define constants
  gamma <- 0.0065 # K/m SI
  M <- 0.0289644 # kg/mol SI
  R <- 8.31432 # Nm/(mol K) SI
  x <- cospi(2*lat/180)
  g <- 9.8063*(1-1E-07*0.5*(Hc+H)*(1-0.0026373*x+5.9E-06*x^2)) #m/s^2 SI

  p <- pc*(((tc-gamma*(H-Hc))/tc)^(g*M/(R*gamma)))


}


load_gnss_database <- function(
  file="gnss_grid.txt",
  datafile,# This is a data.frame now with columns Elevation, Latitude and Longitude
  bbox,#=list(lat=c(34,44.3),
       #     lon=c(-10, 5)),
  save=TRUE,
  #overwrite=FALSE,
  tm.files,
  meteo.files
  ){
  #if(overwrite) file.remove(file)
  #if(file.exists(file)){
  #  gnss.file.grid <- read.table(file, header = TRUE)
  #}else{

    tm.nc <- nc_open(tm.files[1])
    meteo.nc <- nc_open(meteo.files[1])


    gnss.file <- datafile %>% # These are all stations in EUREF
      filter(
        between( Latitude, bbox$lat[1], bbox$lat[2]),
        between(Longitude, bbox$lon[1], bbox$lon[2]))

    # We need to open one file to guess the cells that correspond to each station.
    # Then we can save these values and use them for all the files (all have the same area and sizes)


    lats.meteo <- ncvar_get(meteo.nc, "latitude")
    lons.meteo <- ncvar_get(meteo.nc, "longitude")
    nc_close(meteo.nc)

    lats.tm <- ncvar_get(tm.nc, "latitude")
    lons.tm <- ncvar_get(tm.nc, "longitude")
    nc_close(tm.nc)

    gnss.file.grid <- gnss.file %>%
      filter(!is.na(Latitude)) %>%
      mutate(
        N = map2_dbl(Latitude, Longitude, function(lat, lon) system(paste("echo", lat, lon, "| GeoidEval -n egm2008-1"), intern=TRUE) %>% as.numeric()), # makes use of this tool (install first) https://geographiclib.sourceforge.io/cgi-bin/GeoidEval
        H_geoid = -N + Elevation, # Elevation is height over the ellipsoid, see https://geographiclib.sourceforge.io/cgi-bin/GeoidEval
        gph = gph_from_hgeoid(H_geoid, Latitude),
        g=grav(Latitude),
        newcols = future_map2(Latitude, Longitude, function(Latitude,Longitude){
          x_meteo=which.min(abs(Longitude-lons.meteo))
          y_meteo=which.min(abs(Latitude -lats.meteo))
          Longitude_meteo=lons.meteo[x_meteo]
          Latitude_meteo=lats.meteo[y_meteo]
          sign_x_meteo=sign(Longitude-Longitude_meteo)
          sign_y_meteo=sign(Latitude-Latitude_meteo)*(-1) #Because latitude goes from North to South (decreasing)
          gen_x=c(0,1,0,1)
          gen_y=c(0,0,1,1)
          x_tm=which.min(abs(Longitude-lons.tm))
          y_tm=which.min(abs(Latitude -lats.tm))
          Longitude_tm=lons.tm[x_tm]
          Latitude_tm=lats.tm[y_tm]
          sign_x_tm=sign(Longitude-Longitude_tm)
          sign_y_tm=sign(Latitude-Latitude_tm)*(-1) #Because latitude goes from North to South (decreasing)


          tibble(
            grid=paste0(gen_x,gen_y),
            x_meteo=x_meteo+sign_x_meteo*gen_x,
            y_meteo=y_meteo+sign_y_meteo*gen_y,
            Longitude_meteo=lons.meteo[x_meteo],
            Latitude_meteo=lats.meteo[y_meteo],
            dist.m_meteo=geosphere::distGeo(p1=cbind(Longitude_meteo,Latitude_meteo), p2=cbind(Longitude,Latitude)),
            x_tm=x_tm+sign_x_tm*gen_x,
            y_tm=y_tm+sign_y_tm*gen_y,
            Longitude_tm=lons.tm[x_tm],
            Latitude_tm=lats.tm[y_tm],
            dist.m_tm=geosphere::distGeo(p1=cbind(Longitude_tm,Latitude_tm), p2=cbind(Longitude,Latitude))
          )
        })
      ) %>%
      unnest(c(newcols))
    if(save) write.table(x=gnss.file.grid, file = file, row.names = F)
  #}
  gnss.file.grid
}


get_tm <- function(meteo.files, outputfile, tm.files, gnss.file.grid){
  ## load files ----

  meteo.nc <- nc_open(meteo.files)
  tm.nc    <- nc_open(tm.files)

  ## Load necessary data -----
  time = ncvar_get(meteo.nc, "time") # hours since 1900-01-01 00:00:00
  sp=ncvar_get(meteo.nc, "msl")       # mean sea level pressure hPa
  #sp=ncvar_get(meteo.nc, "sp")       # surface pressure hPa
  z0=ncvar_get(meteo.nc, "z")        # surface geopotential
  t2m=ncvar_get(meteo.nc, "t2m")
  tcwv=ncvar_get(meteo.nc, "tcwv")  # total column water vapor
  # create tibble with all data for each of the 24 hours -------------

  tm.df <- tibble(
    Name    = rep(gnss.file.grid$Name, 24),
    Latitude = rep(gnss.file.grid$Latitude, 24),
    Longitude = rep(gnss.file.grid$Longitude, 24),
    x_meteo = rep(gnss.file.grid$x_meteo, 24),
    y_meteo = rep(gnss.file.grid$y_meteo, 24),
    x_tm = rep(gnss.file.grid$x_tm, 24),
    y_tm = rep(gnss.file.grid$y_tm, 24),
    sp0 = sp[cbind(rep(gnss.file.grid$x_meteo, 24), rep(gnss.file.grid$y_meteo, 24), rep(1:24, each=nrow(gnss.file.grid)))],
    z0 = z0[cbind(rep(gnss.file.grid$x_meteo, 24), rep(gnss.file.grid$y_meteo, 24), rep(1:24, each=nrow(gnss.file.grid)))],
    tcwv = tcwv[cbind(rep(gnss.file.grid$x_meteo, 24), rep(gnss.file.grid$y_meteo, 24), rep(1:24, each=nrow(gnss.file.grid)))],
    h0 = hgeoid_from_gp(z0, Latitude),
    t2m = t2m[cbind(rep(gnss.file.grid$x_meteo, 24), rep(gnss.file.grid$y_meteo, 24), rep(1:24, each=nrow(gnss.file.grid)))],
    #t_msl = t2m - 0.0065*(h0-0), # mean sea level temp.
    g  = rep(gnss.file.grid$g, 24),
    gph  = rep(gnss.file.grid$gph, 24),
    H_geoid = rep(gnss.file.grid$H_geoid, 24),
    Elevation = rep(gnss.file.grid$Elevation, 24),
    time.coord = rep(1:24, each=nrow(gnss.file.grid)),
    time = rep(time, each=nrow(gnss.file.grid))
  )

  # Free memory by removing unnecessary data ----
  rm(sp)
  rm(z0)
  rm(t2m)
  rm(time)
  gc()

  p.levels=tm.nc$dim$level$vals # These are the pressure leves we have

  select.levels <- p.levels #In this version we select all available levels
  #   select.levels <- p.levels %in% c(
  # 1000,
  # 975, 950, 925, 900, 875, 850, 825, 800, 775, 750, 700, 650, 600,
  # 550, 500, 450, 400, 350, 300, 250, 225, 200, 175, 150, 125, 100,
  # 70, 50, 30, 20, 10, 7, 5, 3, 2, 1
  # )
  # Load new set of data (from tm.files) --------------
  g0=9.80665                      # m/s^2, gravity acceleration
  t=ncvar_get(tm.nc, "t")         # Temperature profile in Kelvin
  z=ncvar_get(tm.nc, "z")         # Geopotential profile
  r=ncvar_get(tm.nc, "r")         # Relative humidity
  nc_close(tm.nc) # Close file since we won't use it anymore

  ## And add it to our dataset

  tm.df <- tm.df %>%
    mutate(
      t = pmap(list(x_tm, y_tm, time.coord), function(x,y,time){t[x,y,,time]}),
      z = pmap(list(x_tm, y_tm, time.coord), function(x,y,time){z[x,y,,time]}),
      r = pmap(list(x_tm, y_tm, time.coord), function(x,y,time){r[x,y,,time]}),
      h = pmap(list(z, Latitude), function(z,lat){hgeoid_from_gp(z,lat)})
    )
  ## Remove data that we won't use anymore to save RAM
  rm(t)
  rm(z)
  rm(r)
  gc()

  j <- 0
  tm.df <- tm.df %>% mutate(
    gamma_m = pmap_dbl(list(t, h,H_geoid), function(t,h, H_geoid){

      closest <- which.min(abs(H_geoid-h))
      if(closest==1){
      gamma_m <- mean(c(
        (t[2]-t[1])/(h[2]-h[1]),
        (t[3]-t[2])/(h[3]-h[2])
      ))
      }else{
        gamma_m <- mean(c(
          (t[closest]-t[closest-1])/(h[closest]-h[closest-1]),
          (t[closest+1]-t[closest])/(h[closest+1]-h[closest])
        ))
      }
      gamma_m <- ifelse(between(gamma_m, -10/1000,0), gamma_m, -6.5/1000) #-6.5K/km typical value if we dont believe the ERA5 one.
    }),
    #sp.station = pmap_dbl(list(H_geoid, t, h, Latitude, gamma_m), function(H_geoid, t, h, Latitude, gamma_m){
    sp.station = pmap_dbl(list(H_geoid, t2m, sp0, Latitude, gamma_m), function(H_geoid, t2m, sp0, Latitude, gamma_m){
      #First find the closest level
      #closest <- which.min(abs(H_geoid-h))

      reduce_pressure(#pc    = p.levels[closest],
                      pc    = sp0,
                      #tc    = t[closest],
                      tc    = t2m, # mean temp
                      gamma = gamma_m,
                      H     = H_geoid,
                      #H     = Elevation,
                      #Hc    = h[closest],
                      Hc    = 0,
                      lat   = Latitude) # Reduce pressure to station height

    }),


    new.cols=future_pmap(list(r, h,t,g,H_geoid,sp.station, gamma_m), function(r,h,t,g,H_geoid,sp, gamma_m){
      # First get the Gamma_m (lapse rate) for the first three levels
      # print(length(t))#debugging
      if(h[1]>H_geoid){
        #we have to interpolate
        # once we have a gamma, we have to create new levels every 30m
        seqh <- seq(H_geoid, h[1], 30)
        seqh <- seqh[seqh<h[1]]
        h <- c(h, seqh)
        #z <- h*g
        t <- c(t, ((seqh)- h[1])*gamma_m+t[1])
        r <- c(r, rep(mean(r[1:2]), length(seqh)))
        #pv <- c(pv, rep(pv[1], length(seqh)))
      }else{
        selection <- h>H_geoid
        #z <- z[selection]
        #h <- z/g
        t <- c(approx(x=h, y=t, xout = H_geoid)$y ,t[selection])
        r <- c(approx(x=h, y=r, xout = H_geoid)$y ,r[selection])
        h <- c(H_geoid, h[selection])
      }
      # print(data.frame( #debugging
      #   z=length(z),
      #   h=length(h),
      #   t=length(t),
      #   r=length(r)
      # ))
      tibble(
        #z,
        h,
        t,
        r
      ) %>% arrange(h) %>%
        mutate(
          td = t-273.15,       # Temperature in Celsius
          ps = 6.11 * 10^((7.5 * td)/(237.3+td)), # saturated vapor pressure
          pv = r*ps/100,
          deltah = diff(c(h,last(h)), na.last = FALSE)
        )
    }
    ),
    #tm = map_dbl(new.cols, function(df) with(df, sum(pv*deltah/t, na.rm=TRUE)/sum(pv*deltah/t/t, na.rm=TRUE)))
    #let's try with mean values
    tm = map_dbl(new.cols, function(df){
      with(df %>% mutate(
        pv.m=(pv+lead(pv))/2,
        deltah=c(diff(h), NA),
        t.m=(t+lead(t))/2,
      ),
      sum(pv.m*deltah/t.m, na.rm=TRUE)/sum(pv.m*deltah/t.m/t.m, na.rm=TRUE))
    }
      )
  )
  output <- tm.df %>% select(-new.cols, -t, -r,-z)
  saveRDS(tm.df %>% select(-new.cols, -t, -r,-z), outputfile)
  output
}


getzone <- function(longitude) floor((longitude+180)/6)+1

tmdf2alldf <- function(x, gnssdf){
  #tic()
  #print(x)
#  df <- readRDS(x) %>%
  df <- x %>%
    left_join(
      gnssdf %>%
        select(
          Name,
          Latitude,
          Longitude,
          grid,
          x_meteo,
          y_meteo,
          dist.m_meteo,
          Latitude_meteo,
          Longitude_meteo,
          x_tm,
          y_tm,
          dist.m_tm,
          Latitude_tm,
          Longitude_tm
        )
    )
  df.mean <- df %>%
    mutate(h1=map_dbl(h, function(x){x[1]})) %>%
    select_if(function(x) !is.list(x)) %>%
    group_by(Name, time, time.coord, Latitude, Longitude) %>%
    nest() %>%
    mutate(
      new.data=pmap(list(data, Latitude, Longitude), function(data,lat,lon){
        data <- data %>% filter(!duplicated(.))
        #print(paste(nrow(data), lat, lon, x))
        # data
        # coordinates(data) <- c("Longitude_tm", "Latitude_tm")
        # proj4string(data) <- CRS("+proj=longlat +datum=WGS84")
        # data.sp <- spTransform(data, CRS(paste0("+proj=utm +zone=", getzone(lon), " ellps=WGS84")))
        # data$utm.x <- coordinates(data.sp)[,1]
        # data$utm.y <- coordinates(data.sp)[,2]
        # sta.pos <- data.frame(lon,lat)
        # coordinates(sta.pos) <- c("lon", "lat")
        # proj4string(sta.pos) <- CRS("+proj=longlat +datum=WGS84")
        # sta.pos <- spTransform(sta.pos, CRS(paste0("+proj=utm +zone=", getzone(lon), " ellps=WGS84")))
        dist.x <- distGeo(
          p1=cbind(data$Longitude_tm[data$grid=="00"], data$Latitude_tm[data$grid=="00"]),
          p2=cbind(data$Longitude_tm[data$grid=="10"], data$Latitude_tm[data$grid=="10"]),
        )
        dist.y <- distGeo(
          p1=cbind(data$Longitude_tm[data$grid=="00"], data$Latitude_tm[data$grid=="00"]),
          p2=cbind(data$Longitude_tm[data$grid=="01"], data$Latitude_tm[data$grid=="01"]),
        )
        dist.station <- distGeo(
          p1=cbind(data$Longitude_tm[data$grid=="00"], data$Latitude_tm[data$grid=="00"]),
          p2=rbind(
            cbind(lon, data$Latitude_tm[data$grid=="00"]),
            cbind(data$Longitude_tm[data$grid=="00"], lat)
          )
        )
        data <- data %>%
          mutate(
            pos.x=ifelse(grid=="00" | grid=="01", 0, dist.x),
            pos.y=ifelse(grid=="00" | grid=="10", 0, dist.y),
          ) %>%
          as_tibble()
        if(length(unique(data$pos.x))==1){
          data <- data %>% filter(grid=="00" | grid=="01")
        }else if (length(unique(data$pos.y))==1){
          data <- data %>% filter(grid=="00" | grid=="10")
        }
        #print("crea el interpolado")
        data.1<-data %>% select(-grid) %>%
          summarise_at(
            # .vars=vars(-starts_with("Latitude"),-starts_with("Longitude"),-starts_with("x_"),-starts_with("y_"), -starts_with("pos.")),
            .vars=vars(c("t2m","sp.station","tm","z0", "g","h0","sp0","h1","tcwv")),
            function(x){
              if(length(unique(data$pos.x))==1){

                sol<-interp1(data$pos.y,x, lat)
              }else if (length(unique(data$pos.y))==1){

                sol<-interp1(data$pos.x,x, lon)
              }else{
                m <- xtabs(x~data$pos.x+data$pos.y)
                # print(m)
                sol<-interp2(as.numeric(rownames(m)), as.numeric(colnames(m)), m, dist.station[1], dist.station[2])
              }
              sol
            }
          )
        data.2 <- data %>%
          summarise(
            sp.station.sd=sd(sp.station, na.rm=TRUE),
            tm.sd = sd(tm, na.rm=TRUE)
          )
        bind_cols(data.1, data.2)
      })

    ) %>%
    select(-starts_with("data")) %>%
    unnest(new.data)
  #toc()
  df.mean
}

download_Tm_meteo <- function(root.dir, startdate, enddate, bbox) {
  require(ecmwfr)
  require(lubridate)
  #dates <- seq.Date(from = lubridate::as_date(startdate, origin = lubridate::origin), to = lubridate::as_date(enddate, origin = lubridate::origin), by = "days")
  dates <- seq.Date(from=startdate, to=enddate, by = "days")
  pressure.levels <- c("1000", "975", "950", "925", "900", "875", "850", "825", "800", "775", "750", "700", "650", "600", "550", "500", "450", "400", "350", "300", "250", "225", "200", "175", "150", "125", "100", "70", "50", "30", "20", "10", "7", "5", "3", "2", "1")
  tm.vars <- c("r", "gh", "t", "z")
  meteo.vars <- c("sp", "2m_temperature", "total_column_water_vapour", "z", "msl")
  data_path.tm <- "Tm/"
  data_path.meteo <- "meteo/"
  prefix.tm <- "ERA5_Tm_"
  prefix.meteo <- "ERA5_meteo_"
  suffix <- ".nc"
  for (i in 1:length(dates)) {
    date <- dates[i]
    request.tm <- list(
      dataset_short_name = "reanalysis-era5-pressure-levels",
      product_type = "reanalysis",
      variable = tm.vars,
      pressure_level = pressure.levels,
      year = as.character(lubridate::year(date)),
      month = as.character(lubridate::month(date)),
      day = as.character(lubridate::day(date)),
      time = paste0(sprintf(0:23, fmt = "%02d"), ":00"),
      area = with(bbox, paste(lat[1], lon[1], lat[2], lon[2], sep="/")), # North, West, South, East. Default: global
      format = "netcdf",
      target = paste0(
        data_path.tm, "/",
        format(date, "%Y"), "/",
        format(date, "%m"), "/",
        prefix.tm,
        format(date, "%Y%m%d"),
        suffix
      )
    )
    request.meteo <- request.tm
    request.meteo$dataset_short_name <- "reanalysis-era5-single-levels"
    request.meteo$variable <- meteo.vars
    request.meteo$target <- paste0(
      data_path.meteo, "/",
      format(date, "%Y"), "/",
      format(date, "%m"), "/",
      prefix.meteo,
      format(date, "%Y%m%d"),
      suffix
    )
    request.meteo$pressure_level <- NULL
    #print(str(request.meteo))
    dir.create(paste0(root.dir, dirname(request.meteo$target)), recursive = TRUE)
    dir.create(paste0(root.dir, dirname(request.tm$target)), recursive = TRUE)
    #for (request in c(request.tm, request.meteo)) {
     if(!file.exists(paste0(root.dir, request.meteo$target))) {
       file.meteo <- wf_request( # user     = "1234",   # user ID (for authentification)
        user    = "30911",
        request = request.meteo, # the request
        transfer = TRUE, # download the file
        path = root.dir
     ) # store data in current working directory
     }else print(paste0("download_Tm_meteo: file", request.meteo$target," already exists; skipping"))
    if(!file.exists(paste0(root.dir, request.tm$target))) {
      file.tm <- wf_request( # user     = "1234",   # user ID (for authentification)
        user    = "30911",
        request = request.tm, # the request
        transfer = TRUE, # download the file
        path = root.dir
      ) # store data in current working directory
    }else print(paste0("download_Tm_meteo: file", request.tm$target," already exists; skipping"))
    }
      #list(meteo=file.meteo, tm=file.tm)
}

read_network <- function(filename, source){
  require(dplyr)
    if(source=="EUREF") filename %>%
      read.csv()
    else if(source=="Nevada") filename %>%
      read.table(header = TRUE, fill = TRUE) %>%
      rename(Latitude="Lat.deg.",
             Longitude="Long.deg.",
             Elevation="Hgt.m.",
             Name="Sta") %>%
      mutate(Longitude=ifelse(Longitude>180, -360+Longitude, Longitude),
             Latitude=as.numeric(as.character(Latitude)))
    else stop("source must be either \"EUREF\" or \"Nevada\"")
}


startend2timeseries <- function(.data, start, end, x, y){
  frame_list <- Map(seq, from = .data$`{{ start }}`, to = .data$`{{ end }}`)
  DF <- data.frame("{{ x }}" := unlist(Map(rep, df$`{{ x }}`, lengths(frame_list))),
                   "{{ y }}" := unlist(Map(rep, df$`{{ y }}`, lengths(frame_list))),
                   frame = unlist(frame_list))
}
